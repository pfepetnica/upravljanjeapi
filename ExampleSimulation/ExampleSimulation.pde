// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float onoCimeUpravljate = 0;
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float onoStoMerite = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{
  float nekaVrednost = 0;

  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);
    
    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);
    
    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    nekaVrednost = nekaVrednost + 1;
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.onoStoMerite = nekaVrednost;
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    o.onoCimeUpravljate = 1;
    
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    background(204);
    stroke(255, 0, 0);
    line(0, 100, width, 100); 
    stroke(255);
    line(0, nekaVrednost, width, nekaVrednost);
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);
  
  sim = new MojaSimulacija();
}